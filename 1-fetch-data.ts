import * as wmill from "https://deno.land/x/windmill@v1.116.0/mod.ts";

export async function main() {
  // 1. Get the last saved state
  const state = await wmill.getState();

  // 2. Get the actual state from the external service
  let url = "";
  if (state?.nextUrl) {
    url = state.nextUrl;
  } else {
    const queryParams = new URLSearchParams({
      format: "json",
      product: "thunderbird",
      created__gt: "2023-06-09",
      //created__lt: "2023-06-09",
      // URLSearchParams().toString() escapes '+' which breaks the ordering of the API.
      //ordering: "+created",
    }).toString();
    url =
      `https://support.mozilla.org/api/2/question/?${queryParams}&ordering=+created`;
  }

  const data = await (await fetch(url)).json();
  //console.log(data);
  const results: { id: number }[] = data.results;
  const newQuestions = results.filter((event) =>
    event.id > (state?.latestId ?? 0)
  ).sort((a, b) => a.id - b.id);
  const ids: number[] = results.map((event) => event.id);

  // 3. Compare the two states and update the internal state
  const newState = {
    latestId: ids.length > 0 ? Math.max(...ids) : state?.latestId,
    nextUrl: data.next ?? state.nextUrl,
  };
  await wmill.setState(newState);
  console.log(JSON.stringify(
    {
      state,
      newState,
      questions: data.results.map((question: any) => question.id),
      newQuestions: newQuestions.map((question: any) => question.id),
    },
    null,
    2,
  ));

  // 4. Return the new rows
  return newQuestions;
}
