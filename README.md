# Thunderbird Support Matrix Bot - Windmill

Get notified about new Thunderbird Support questions in a Matrix room. A team of supporters may use the Matrix room to coordinate and discuss replies.

![](./docs/matrix-room-preview.png)

## The flow

1. Fetch new questions from the [Thunderbird's public support forum](https://support.mozilla.org/en-US/questions/thunderbird).
2. Post new questions (link + initial question) in a Matrix room.

It does not:
* Allow to respond to questions within Matrix.
* Update edited or deleted questions.
* Use the End-to-End-Encryption of Matrix. They are public anyway.
* Post new questions as soon as they get posted. As far as I know the Thunderbird Support forum has no webhook support. Instead, this flow should be run on a schedule.

![](./docs/flow.png)

## Setup

Use [Windmill](https://windmill.dev) to run this flow.

1. Create a Matrix resource. (homeserver address + access token of a Matrix account)
2. Import the flow in `flow.json`.
3. Add a schedule for the flow. I chose to run it every 30 minutes.

![](./docs/schedule.png)

## License

[MIT No Attribution License (MIT-0)](https://opensource.org/license/mit-0/)
