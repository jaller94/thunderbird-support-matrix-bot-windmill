import * as wmill from "https://deno.land/x/windmill@v1.116.0/mod.ts";

export async function main(
  matrix_res: wmill.Resource<"matrix">,
  room: string,
  id: number,
  title: string,
  content: string,
) {
  if (!matrix_res.token) {
    throw Error("Sending a message requires an access token.");
  }
  const rootEventId = await sendRootMessage(matrix_res, room, id, title);
  await sendContentMessage(matrix_res, room, rootEventId, content);
}

async function sendRootMessage(
  matrix_res: wmill.Resource<"matrix">,
  room: string,
  id: number,
  title: string,
) {
  if (!matrix_res.token) {
    throw Error("Sending a message requires an access token.");
  }
  const txnId = `${Math.random()}`;
  const resp = await fetch(
    `${matrix_res.baseUrl}/_matrix/client/v3/rooms/${
      encodeURIComponent(room)
    }/send/m.room.message/${txnId}`,
    {
      method: "PUT",
      headers: {
        "Accept": "application/json",
        "Authorization": `Bearer ${matrix_res.token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        body:
          `New question: https://support.mozilla.org/en-US/questions/${id} title: ${title}`,
        format: "org.matrix.custom.html",
        formatted_body:
          `New question: id: <a href=\"https://support.mozilla.org/en-US/questions/${id}\">${id}</a> title: ${title}`,
        msgtype: "m.text",
      }),
    },
  );
  if (!resp.ok) {
    throw Error(`Failed to send message: Error HTTP${resp.status} ${await resp.text()}`);
  }
  const eventId = (await resp.json())["event_id"];
  if (typeof eventId !== "string") {
    throw Error(
      `Faulty Matrix server implementation: Server didn't provide event_id for this message.`,
    );
  }
  return eventId;
}

async function sendContentMessage(
  matrix_res: wmill.Resource<"matrix">,
  room: string,
  rootEventId: string,
  content: string,
) {
  if (!matrix_res.token) {
    throw Error("Sending a message requires an access token.");
  }
  const txnId = `${Math.random()}`;
  const resp = await fetch(
    `${matrix_res.baseUrl}/_matrix/client/v3/rooms/${
      encodeURIComponent(room)
    }/send/m.room.message/${txnId}`,
    {
      method: "PUT",
      headers: {
        "Accept": "application/json",
        "Authorization": `Bearer ${matrix_res.token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        body: content,
        msgtype: "m.text",
        format: "org.matrix.custom.html",
        formatted_body: content,
        "m.relates_to": {
          event_id: rootEventId,
          is_falling_back: true,
          "m.in_reply_to": {
            "event_id": rootEventId,
          },
          "rel_type": "m.thread",
        },
      }),
    },
  );
  if (!resp.ok) {
    throw Error(`Failed to send message: Error HTTP${resp.status} ${await resp.text()}`);
  }
  const eventId = (await resp.json())["event_id"];
  if (typeof eventId !== "string") {
    throw Error(
      `Faulty Matrix server implementation: Server didn't provide event_id for this message.`,
    );
  }
  return eventId;
}
